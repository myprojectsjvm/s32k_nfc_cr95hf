/**
  ******************************************************************************
  * @file    drv_interrupt.c 
  * @author  MMY Application Team
  * @version V4.0.0
  * @date    02/06/2014
  * @brief   This file configured the interruption raised by 95HF device.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2014 STMicroelectronics</center></h2>
  *
  * Licensed under MMY-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
	******************************************************************************
  */ 

#include "drv_interrupt.h"

/** @addtogroup _95HF_Libraries
 * 	@{
 *	@brief  <b>This is the library used by the whole 95HF family (RX95HF, CR95HF, ST95HF) <br />
 *				  You will find ISO libraries ( 14443A, 14443B, 15693, ...) for PICC and PCD <br />
 *				  The libraries selected in the project will depend of the application targetted <br />
 *				  and the product chosen (RX95HF emulate PICC, CR95HF emulate PCD, ST95HF can do both)</b>
 */

/** @addtogroup _95HF_Driver
 * 	@{
 *  @brief  <b>This folder contains the driver layer of 95HF family (CR95HF, RX95HF, ST95HF)</b>
 */

/** @addtogroup drv_interrupt
 * 	@{
 *  @brief  This file contains the ressource configuration needed by the 95HF <br />  
 *  			  GPIO configuration is inside drv_interrupt header file <br /> 
 *  			  95HF driver use 1 timeout (need 1 TIMER resource from MCU)
 */
 
/**
 *	@brief  this uTimeOut variable is used as a timeout duting the communication with the RF tranceiver
 */
extern __IO uint8_t						uTimeOut;
extern volatile bool										uAppliTimeOut;

extern volatile bool RF_DataExpected;
extern volatile bool uDataReady;
 
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
uint16_t delay_appli = 0;
uint16_t delay_timeout = 0;
/* Private functions Prototype -----------------------------------------------*/

/** @addtogroup drv_interrupt_Private_Functions
 * 	@{
 */

/**
  * @}
  */

/** @addtogroup drv_interrupt_Public_Functions
 * 	@{
 */

/**
 *	@brief  this function sends a negative pulse on SPI_NSS pin
 */
void SendSPINSSPulse(void)
{
		RFTRANS_95HF_NSS_HIGH() ;
		delayHighPriority_ms(1);
		RFTRANS_95HF_NSS_LOW() ;
		delayHighPriority_ms(1);
		RFTRANS_95HF_NSS_HIGH() ;
}

/**
 *	@brief  This function configures the Extern Interrupt for the IRQ coming from the RF transceiver
 */
void drvInt_Enable_Reply_IRQ( void )
{
	
	/** @todo CR95HF Input interrupt enable. */

}

/**
 *	@brief  This function configures the Extern Interrupt for the IRQ coming from the RF transceiver
 */
void drvInt_Enable_RFEvent_IRQ( void )
{
	
	/** @todo CR95HF Input interrupt enable. */

}

/**
 *	@brief  This function configures the Extern Interrupt for the IRQ coming from the RF transceiver
 */
void drvInt_Disable_95HF_IRQ( void )
{
	
	/** @todo CR95HF Input interrupt disable. */

}

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/**
 * @}
 */ 


/******************* (C) COPYRIGHT 2014 STMicroelectronics *****END OF FILE****/


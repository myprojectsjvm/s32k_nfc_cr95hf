/**
  ******************************************************************************
  * @file    drv_95HF.c 
  * @author  MMY Application Team
  * @version V4.0.0
  * @date    02/06/2014
  * @brief   This file provides set of driver functions to manage communication 
  * @brief   between MCU and xx95HF chip (RX95HF, CR95HF or ST95HF) 
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2014 STMicroelectronics</center></h2>
  *
  * Licensed under MMY-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
	******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------------------ */
#include "drv_95HF.h"

/** @addtogroup _95HF_Libraries
 * 	@{
 *	@brief  <b>This is the library used by the whole 95HF family (RX95HF, CR95HF, ST95HF) <br />
 *				  You will find ISO libraries ( 14443A, 14443B, 15693, ...) for PICC and PCD <br />
 *				  The libraries selected in the project will depend of the application targetted <br />
 *				  and the product chosen (RX95HF emulate PICC, CR95HF emulate PCD, ST95HF can do both)</b>
 */
 
/** @addtogroup _95HF_Driver
 * 	@{
 *  @brief  <b>This folder contains the driver layer of 95HF family (CR95HF, RX95HF, ST95HF)</b>
 */

/** @addtogroup drv_95HF
 * 	@{
 *  @brief  This file includes the driver of the xx95HF family (CR95HF, RX95HF, ST95HF)
 */

/**
* @brief  buffer to exchange data with the RF tranceiver.
*/
uint8_t				u95HFBuffer [RFTRANS_95HF_MAX_BUFFER_SIZE+3];

/** 
 *  @brief This uTimeOut variable is used as a timeout duting the communication with the RF tranceiver 
 */
extern volatile bool							uDataReady; 
__IO uint8_t						uTimeOut;
bool EnableTimeOut = true;


/* ConfigStructure */ 										 
drv95HF_ConfigStruct			drv95HFConfig;

/* drv95HF_Private_Functions */
static void drv95HF_SendSPIResetByte				( void );

static int8_t drv95HF_SPIPollingCommand			( void );



/** @addtogroup drv_95HF_Private_Functions
 *  @{
 */


/**
 *	@brief  This function sends a reset command over SPI bus
 *  @param  none
 *  @retval None
 */
static void drv95HF_SendSPIResetByte(void)
{
	/* Send reset control byte */
	SPI_SendReceiveByte(RFTRANS_95HF_SPI, RFTRANS_95HF_COMMAND_RESET);
}


/**
 * @}
 */

/** @addtogroup drv_95HF_Public_Functions
 * 	@{
 */

/**
* @brief  	Initilize the 95HF device config structure
* @param  	None
* @retval 	None
*/
void drv95HF_InitConfigStructure (void)
{
	drv95HFConfig.uInterface = RFTRANS_95HF_INTERFACE_SPI;
	drv95HFConfig.uSpiMode = RFTRANS_95HF_SPI_POLLING;
	drv95HFConfig.uState = RFTRANS_95HF_STATE_POWERUP;
	drv95HFConfig.uCurrentProtocol = RFTRANS_95HF_PROTOCOL_UNKNOWN;
	drv95HFConfig.uMode = RFTRANS_95HF_MODE_UNKNOWN;
}


/**
 *	@brief  Send a reset sequence over SPI bus (Reset command ,wait ,negative pulse on IRQin).
 *  @param  None
 *  @retval None
 */
void drv95HF_ResetSPI ( void )
{	
	/* Deselect Rftransceiver over SPI */
	RFTRANS_95HF_NSS_HIGH();
	delayHighPriority_ms(1);
	/* Select 95HF device over SPI */
	RFTRANS_95HF_NSS_LOW();
	/* Send reset control byte	*/
	drv95HF_SendSPIResetByte();
	/* Deselect 95HF device over SPI */
	RFTRANS_95HF_NSS_HIGH();
	delayHighPriority_ms(3);

	/* send a pulse on IRQ_in to wake-up 95HF device */
	drv95HF_SendIRQINPulse();
	delayHighPriority_ms(10);  /* mandatory before issuing a new command */

	drv95HFConfig.uState = RFTRANS_95HF_STATE_READY;
	
}

/**  
 *  @brief  This function returns the IRQout state
 *  @param  None
 *  @retval Pin set : 1
 *  @retval Pin reset : 0
 */
int8_t drv95HF_GetIRQOutState ( void )
{

	/** @todo CR95HF Input interrupt status. */

	return 0;

}

/**
 *	@brief  This function initialize MCU serial interface peripheral (SPI or UART)
 *  @param  None
 *  @retval None
 */
void drv95HF_InitilizeSerialInterface ( void )
{
	drv95HFConfig.uInterface = RFTRANS_95HF_INTERFACE_SPI;
}

/**
 *	@brief  This function enable the interruption
 *  @param  None
 *  @retval None
 */
void drv95HF_EnableInterrupt(void)
{
	/* enable interruption */
	drvInt_Enable_Reply_IRQ();
	
	/* set back driver in polling mode */
	drv95HFConfig.uSpiMode = RFTRANS_95HF_SPI_INTERRUPT;	
	
}

/**
 *	@brief  This function disable the interruption
 *  @param  None
 *  @retval None
 */
void drv95HF_DisableInterrupt(void)
{
	/* disable interruption */
	drvInt_Disable_95HF_IRQ();
	
	/* set back driver in polling mode */
	drv95HFConfig.uSpiMode = RFTRANS_95HF_SPI_POLLING;	
}


/**
 *	@brief  This function returns the Interface selected(UART or SPI)
 *  @param  none
 *  @retval RFTRANS_INTERFACE_UART : the UART interface is selected
 *  @retval RFTRANS_INTERFACE_SPI : the SPI interface is selected
 */
uint8_t drv95HF_GetSerialInterface ( void )
{
	return drv95HFConfig.uInterface;
}

/**
 *	@brief  This function sends a command over SPI bus
 *  @param  *pData : pointer on data to send to the xx95HF
 *  @retval void
 */
void drv95HF_SendSPICommand(uc8 *pData)
{
	uint8_t DummyBuffer[MAX_BUFFER_SIZE];
  uint16_t bufferlength = 0;
	  	
	/*  Select xx95HF over SPI  */
	RFTRANS_95HF_NSS_LOW();

	/* Send a sending request to xx95HF  */
	SPI_SendReceiveByte(RFTRANS_95HF_SPI, RFTRANS_95HF_COMMAND_SEND);

	if(*pData == ECHO)
	{
		/* Send a sending request to xx95HF */ 
		SPI_SendReceiveByte(RFTRANS_95HF_SPI, ECHO);
	}
	else
	{
    if( pData[RFTRANS_95HF_COMMAND_OFFSET] == 0x24 )
    {
      bufferlength = pData[RFTRANS_95HF_LENGTH_OFFSET] + RFTRANS_95HF_DATA_OFFSET + 256;
    }
    else if (pData[RFTRANS_95HF_COMMAND_OFFSET] == 0x44)
    {
      bufferlength = pData[RFTRANS_95HF_LENGTH_OFFSET] + RFTRANS_95HF_DATA_OFFSET + 512;
    }
    else
    {
      bufferlength = pData[RFTRANS_95HF_LENGTH_OFFSET] + RFTRANS_95HF_DATA_OFFSET;
    }

		/* Transmit the buffer over SPI */
#ifdef USE_DMA	
		SPI_SendReceiveBufferDMA(RFTRANS_95HF_SPI, pData, bufferlength, DummyBuffer);
#else
		SPI_SendReceiveBuffer(RFTRANS_95HF_SPI, pData, bufferlength, DummyBuffer);
#endif
	}
	
	/* Deselect xx95HF over SPI  */
	RFTRANS_95HF_NSS_HIGH();
}

/**
 *	@brief  This function polls 95HF chip until a response is ready or
 *				  the counter of the timeout overflows
 *  @retval PCD_POLLING_TIMEOUT : The time out was reached 
 *  @retval PCD_SUCCESS_CODE : A response is available
 */
static int8_t drv95HF_SPIPollingCommand( void )
{
	uint8_t Polling_Status = 0;

	/** @todo CR95HF Timeout. */

	if (drv95HFConfig.uSpiMode == RFTRANS_95HF_SPI_POLLING)
	{
		
		do{
			
			RFTRANS_95HF_NSS_LOW();
			
			delay_ms(2);
					
			/*  poll the 95HF transceiver until he's ready ! */
			Polling_Status  = SPI_SendReceiveByte(RFTRANS_95HF_SPI, RFTRANS_95HF_COMMAND_POLLING);
			
			Polling_Status &= RFTRANS_95HF_FLAG_DATA_READY_MASK;
	
		}	while( Polling_Status 	!= RFTRANS_95HF_FLAG_DATA_READY );
		
		RFTRANS_95HF_NSS_HIGH();
	}	
	else if (drv95HFConfig.uSpiMode == RFTRANS_95HF_SPI_INTERRUPT)
	{

	}

	if ( uTimeOut == true )
		return RFTRANS_95HF_POLLING_TIMEOUT;

	return RFTRANS_95HF_SUCCESS_CODE;	
}

/**
 *	@brief  This fucntion recovers a response from 95HF device
 *  @param  *pData : pointer on data received from 95HF device
 *  @retval None
 */
void drv95HF_ReceiveSPIResponse(uint8_t *pData)
{
	uint8_t DummyBuffer[MAX_BUFFER_SIZE];
  uint16_t lengthToRead = 0;

	/* Select 95HF transceiver over SPI */
	RFTRANS_95HF_NSS_LOW();

	/* Request a response from 95HF transceiver */
	SPI_SendReceiveByte(RFTRANS_95HF_SPI, RFTRANS_95HF_COMMAND_RECEIVE);

	/* Recover the "Command" byte */
	pData[RFTRANS_95HF_COMMAND_OFFSET] = SPI_SendReceiveByte(RFTRANS_95HF_SPI, DUMMY_BYTE);

	if(pData[RFTRANS_95HF_COMMAND_OFFSET] == ECHO)
	{
		pData[RFTRANS_95HF_LENGTH_OFFSET]  = 0x00;
		/* In case we were in listen mode error code cancelled by user (0x85 0x00) must be retrieved */
		pData[RFTRANS_95HF_LENGTH_OFFSET+1] = SPI_SendReceiveByte(RFTRANS_95HF_SPI, DUMMY_BYTE);
		pData[RFTRANS_95HF_LENGTH_OFFSET+2] = SPI_SendReceiveByte(RFTRANS_95HF_SPI, DUMMY_BYTE);
	}
	else if(pData[RFTRANS_95HF_COMMAND_OFFSET] == 0xFF)
	{
		pData[RFTRANS_95HF_LENGTH_OFFSET]  = 0x00;
		pData[RFTRANS_95HF_LENGTH_OFFSET+1] = SPI_SendReceiveByte(RFTRANS_95HF_SPI, DUMMY_BYTE);
		pData[RFTRANS_95HF_LENGTH_OFFSET+2] = SPI_SendReceiveByte(RFTRANS_95HF_SPI, DUMMY_BYTE);
	}
	else
	{
		/* Recover the "Length" byte */
		pData[RFTRANS_95HF_LENGTH_OFFSET]  = SPI_SendReceiveByte(RFTRANS_95HF_SPI, DUMMY_BYTE);
		/* Checks the data length */
		if( !( ( ( pData[RFTRANS_95HF_COMMAND_OFFSET] & 0xE0) == 0x80 ) && (pData[RFTRANS_95HF_LENGTH_OFFSET] == 0x00) ) )
    {
      lengthToRead = (uint16_t)(pData[RFTRANS_95HF_COMMAND_OFFSET] & 0x60);
      lengthToRead = (lengthToRead << 3) + pData[RFTRANS_95HF_LENGTH_OFFSET];
			/* Recover data 	*/
#ifdef USE_DMA
      SPI_SendReceiveBufferDMA(RFTRANS_95HF_SPI, DummyBuffer, lengthToRead, &pData[RFTRANS_95HF_DATA_OFFSET]);    
#else
      SPI_SendReceiveBuffer(RFTRANS_95HF_SPI, DummyBuffer, lengthToRead, &pData[RFTRANS_95HF_DATA_OFFSET]);
#endif	
		}
	}

	/* Deselect xx95HF over SPI */
	RFTRANS_95HF_NSS_HIGH();
	
}


/**
 *	@brief  This function send a command to 95HF device over SPI or UART bus and receive its response
 *  @param  *pCommand  : pointer on the buffer to send to the 95HF device ( Command | Length | Data)
 *  @param  *pResponse : pointer on the 95HF device response ( Command | Length | Data)
 *  @retval RFTRANS_95HF_SUCCESS_CODE : the function is succesful
 */
int8_t  drv95HF_SendReceive(uc8 *pCommand, uint8_t *pResponse)
{		
	u8 command = *pCommand;
	
	/* if we want to send a command we are not expected a interrupt from RF event */
	if(drv95HFConfig.uSpiMode == RFTRANS_95HF_SPI_INTERRUPT)
	{	
		drvInt_Enable_Reply_IRQ();
	}
	
	if(drv95HFConfig.uInterface == RFTRANS_95HF_INTERFACE_SPI)
	{
		/* First step  - Sending command 	*/
		drv95HF_SendSPICommand(pCommand);
		/* Second step - Polling	*/
		if (drv95HF_SPIPollingCommand( ) != RFTRANS_95HF_SUCCESS_CODE)
		{	*pResponse =RFTRANS_95HF_ERRORCODE_TIMEOUT;
			return RFTRANS_95HF_POLLING_TIMEOUT;	
		}
		/* Third step  - Receiving bytes */
		drv95HF_ReceiveSPIResponse(pResponse);
	}
#ifdef CR95HF	
	else if(drv95HFConfig.uInterface == RFTRANS_95HF_INTERFACE_UART)
	{
	}
#endif /* CR95HF */
	
	/* After listen command is sent an interrupt will raise when data from RF will be received */
	if(command == LISTEN)
	{	
		if(drv95HFConfig.uSpiMode == RFTRANS_95HF_SPI_INTERRUPT)
		{		
			drvInt_Enable_RFEvent_IRQ( );
		}
	}

	return RFTRANS_95HF_SUCCESS_CODE; 
}

/**
 *	@brief  This function send a command to 95HF device over SPI or UART bus
 *  @param  *pCommand  : pointer on the buffer to send to the 95HF ( Command | Length | Data)
 *  @retval None
 */
void drv95HF_SendCmd(uc8 *pCommand)
{
	if(drv95HFConfig.uInterface == RFTRANS_95HF_INTERFACE_SPI)
		/* First step  - Sending command 	*/
		drv95HF_SendSPICommand(pCommand);
#ifdef CR95HF	
	else if(drv95HFConfig.uInterface == RFTRANS_95HF_INTERFACE_UART);
#endif /* CR95HF */
}

/**
 *	@brief  This function is a specific command. It's made polling and reading sequence. 
 *  @param  *pResponse : pointer on the 95HF device response ( Command | Length | Data)
 *  @retval RFTRANS_95HF_SUCCESS_CODE : the function is succesful
 *  @retval RFTRANS_95HF_POLLING_RFTRANS_95HF : the polling sequence returns an error
 */
int8_t  drv95HF_PoolingReading (uint8_t *pResponse)
{
	
	*pResponse =RFTRANS_95HF_ERRORCODE_DEFAULT;
	*(pResponse+1) = 0x00;

	if(drv95HFConfig.uInterface == RFTRANS_95HF_INTERFACE_SPI)
	{
		/* First step - Polling	*/
		if (drv95HF_SPIPollingCommand( ) != RFTRANS_95HF_SUCCESS_CODE)
		{	*pResponse = RFTRANS_95HF_ERRORCODE_TIMEOUT;
			return RFTRANS_95HF_ERRORCODE_TIMEOUT;	
		}
		
		/* Second step  - Receiving bytes 	*/
		drv95HF_ReceiveSPIResponse(pResponse);
	}
#ifdef CR95HF	
	else if(drv95HFConfig.uInterface == RFTRANS_95HF_INTERFACE_UART)
	{
	}
#endif /* CR95HF */
	return RFTRANS_95HF_SUCCESS_CODE; 
}

/**
 *	@brief  Send a negative pulse on IRQin pin
 *  @param  none
 *  @retval None
 */
void drv95HF_SendIRQINPulse(void)
{
	if (drv95HFConfig.uInterface == RFTRANS_95HF_INTERFACE_SPI)
	{
		/* Send a pulse on IRQ_IN */
		RFTRANS_95HF_IRQIN_HIGH() ;
		delayHighPriority_ms(1);
		RFTRANS_95HF_IRQIN_LOW() ;
		delayHighPriority_ms(1);
		RFTRANS_95HF_IRQIN_HIGH() ;
	}
	
	/* Need to wait 10ms after the pulse before to send the first command */
	delayHighPriority_ms(10);

}

/**
 *	@brief  this functions put the ST95HF in sleep/hibernate mode
 *  @param  WU_source : Source selected to wake up the device (WU_TIMEOUT,WU_TAG,WU_FIELD,WU_IRQ,WU_SPI)
 *  @param  mode : Can be IDLE_SLEEP_MODE or IDLE_HIBERNATE_MODE
 *  @retval None 
 */
void drv95HF_Idle(uc8 WU_source, uc8 mode)
{
	uint8_t pCommand[] = {RFTRANS_95HF_COMMAND_IDLE, IDLE_CMD_LENTH, 0, 0, 0, 0, 0 ,0x18 ,0x00 ,0x00 ,0x60 ,0x00 ,0x00 ,0x00 ,0x00 ,0x00};

	if (mode == IDLE_SLEEP_MODE) /* SLEEP */
	{
		/* Select the wake up source*/
		pCommand[2] = WU_source;
		/* Select SLEEP mode */
		if (WU_source == WU_FIELD)
		{
			pCommand[3] = GETMSB(SLEEP_FIELD_ENTER_CTRL);
			pCommand[4] = GETLSB(SLEEP_FIELD_ENTER_CTRL);
		}
		else
		{
			pCommand[3] = GETMSB(SLEEP_ENTER_CTRL);
			pCommand[4] = GETLSB(SLEEP_ENTER_CTRL);
		}
		pCommand[5] = GETMSB(SLEEP_WU_CTRL);
		pCommand[6] = GETLSB(SLEEP_WU_CTRL);
	}
	else /* HIBERNATE */
	{
		/* Select the wake up source, only IRQ is available for HIBERNATE mode*/
		pCommand[2] = WU_IRQ;
		/* Select HIBERNATE mode */
		pCommand[3] = GETMSB(HIBERNATE_ENTER_CTRL);
		pCommand[4] = GETLSB(HIBERNATE_ENTER_CTRL);
		pCommand[5] = GETMSB(HIBERNATE_WU_CTRL);
		pCommand[6] = GETLSB(HIBERNATE_WU_CTRL);
		pCommand[10] = 0x00;
	}
	
	/* Send the command */
	if(drv95HFConfig.uInterface == RFTRANS_95HF_INTERFACE_SPI)
		/* First step  - Sending command 	*/
		drv95HF_SendSPICommand(pCommand);
#ifdef CR95HF	
	else if(drv95HFConfig.uInterface == RFTRANS_95HF_INTERFACE_UART);
#endif /* CR95HF */
	
}


/**
 * @}
 */ 

/**
 * @}
 */
/**
 * @}
 */

/**
 * @}
 */

/******************* (C) COPYRIGHT 2014 STMicroelectronics *****END OF FILE****/


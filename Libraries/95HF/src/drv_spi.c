/**
  ******************************************************************************
  * @file    drv_spi.c 
  * @author  MMY Application Team
  * @version V4.0.0
  * @date    02/06/2014
  * @brief   This file provides a set of firmware functions to manages SPI communications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2014 STMicroelectronics</center></h2>
  *
  * Licensed under MMY-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
	******************************************************************************
*/ 

/* Includes ------------------------------------------------------------------*/
#include "drv_spi.h"

/** @addtogroup _95HF_Libraries
 * 	@{
 *	@brief  <b>This is the library used by the whole 95HF family (RX95HF, CR95HF, ST95HF) <br />
 *				  You will find ISO libraries ( 14443A, 14443B, 15693, ...) for PICC and PCD <br />
 *				  The libraries selected in the project will depend of the application targetted <br />
 *				  and the product chosen (RX95HF emulate PICC, CR95HF emulate PCD, ST95HF can do both)</b>
 */

/** @addtogroup _95HF_Driver
 * 	@{
 *  @brief  <b>This folder contains the driver layer of 95HF family (CR95HF, RX95HF, ST95HF)</b>
 */

/** @addtogroup drv_SPI
 *  @{
 *  @brief  This file includes the SPI driver used by xx95HF family (CR95HF, RX95HF, ST95HF)
 *          to communicate with the MCU.
 */
 
 
/** @addtogroup drv_SPI_Private_Functions
 * 	@{
 */


 
 /**
  * @}
  */

/** @addtogroup drv_SPI_Public_Functions
 * 	@{
 */

/**  
 *	@brief  Sends one byte over SPI and recovers a response
 *  @param  SPIx : where x can be 1, 2 or 3 to select the SPI peripheral
 *  @param  data : data to send
 *  @retval data response from SPIx 
 */
uint8_t SPI_SendReceiveByte(uint32_t instance, uint8_t data)
{	

	uint8_t dummy;

	LPSPI_DRV_MasterTransferBlocking(instance, &data, NULL, 1, 100);
	LPSPI_DRV_MasterTransferBlocking(instance, &dummy, &data, 1, 100);

	return data;

}


/**
 *	@brief  reveive a byte array over SPI
 *  @param  SPIx	 	: where x can be 1, 2 or 3 to select the SPI peripheral
 *  @param  pCommand  	: pointer on the buffer to send
 *  @param  length	 	: length of the buffer to send
 *  @param  pResponse 	: pointer on the buffer response
 *  @retval None 
 */
void SPI_SendReceiveBuffer(uint32_t instance, uc8 *pCommand, uint16_t length, uint8_t *pResponse)
{
	uint16_t i;
	
	/* the buffer size is limited to SPI_RESPONSEBUFFER_SIZE */
	length = MIN (SPI_RESPONSEBUFFER_SIZE,length);
	for(i=0; i<length; i++)
		pResponse[i] = SPI_SendReceiveByte(instance, pCommand[i]);
}


/**
 * @}
 */ 

/**
 * @}
 */ 

/**
 * @}
 */ 

/**
 * @}
 */ 
/******************* (C) COPYRIGHT 2014 STMicroelectronics *****END OF FILE****/

/***************************************************************************//**
	@file		app.c
	@brief		Application used to test the CR95HF NFC reader.
********************************************************************************
	Application used to test the CR95HF NFC reader. This firmware was based
	in the STSW-M24LR007 software offere by ST.

	@see https://www.st.com/content/st_com/en/products/evaluation-tools/product-evaluation-tools/st25-nfc-rfid-eval-tools/st25-nfc-rfid-eval-boards/m24lr-discovery.html#tools-software

	@see https://www.st.com/content/st_com/en/products/embedded-software/st25-nfc-rfid-software/stsw-m24lr007.html

	Examples using @ref drv95HF_SendReceive routine.

	Inventory:
		MCU -> CR95HF : 0x04 0x03 0x26 0x01 0x00
		CR95HF -> MCU : 0x80 0x0D 0x00 0xFF 0x7C 0x3F 0xC9 0xB9 0x9A 0x58 0x02 0xE0 0xF2 0x2D 0x00

	Write Single Block:
		MCU -> CR95HF : 0x04 0x07 0x02 0x21 0x00 0x01 0x02 0x03 0x04
		CR95HF -> MCU : 0x80 0x04 0x00 0x78 0xF0 0x00
		MCU -> CR95HF : 0x04 0x07 0x02 0x21 0x01 0x01 0x02 0x03 0x04
		CR95HF -> MCU : 0x80 0x04 0x00 0x78 0xF0 0x00

	Read Single Block:
		MCU -> CR95HF : 0x04 0x03 0x02 0x20 0x00
		CR95HF -> MCU : 0x80 0x08  0x00 0x01 0x02 0x03 0x04 0x38 0x0A 0x00
		MCU -> CR95HF : 0x04 0x03 0x02 0x20 0x01
		CR95HF -> MCU : 0x80 0x08  0x00 0x01 0x02 0x03 0x04 0x38 0x0A 0x00

	@note		S32 Design Studio for ARM.

	@note		S32K microcontrollers.

	@see		http://www.doxygen.nl/index.html

********************************************************************************
	@author		Jose Vinicius Melo.
	@date		February-2019.
*******************************************************************************/

#include "app.h"
#include "defines.h"
#include "lib_ConfigManager.h"

/*******************************************************************************
	SETTINGS:
*******************************************************************************/

/*******************************************************************************
	DEFINES:
*******************************************************************************/

#define CR95HF_ADD	0x00

/*******************************************************************************
	TYPEDEFS:
*******************************************************************************/

/*******************************************************************************
	GLOBALS VARIABLES:
*******************************************************************************/

/*******************************************************************************
	CONSTANTS:
*******************************************************************************/

const uint8_t Cr95hfCmdInventory[] = {0x04, 0x03, 0x26, 0x01, 0x00};
const uint8_t Cr95hfCmdWriteSingleBlock[] = {0x04, 0x07, 0x02, 0x21, CR95HF_ADD, 0x01, 0x02, 0x03, 0x04};
const uint8_t Cr95hcCmdReadSingleBlock[] = {0x04, 0x03, 0x02, 0x20, 0x00};

/*******************************************************************************
	LOCAL PROTOTYPES:
*******************************************************************************/

/***************************************************************************//**
	@brief		Main application routine.
	@param		None.
	@return		None.
*******************************************************************************/

void App_Main(void) {

	uint8_t resp[0x20E], flag = 0;

	/* Initializations: */
	uint8_t TagType = TRACK_NOTHING, tagfounds=TRACK_ALL;

	/* Initialize and configure clocks: */
	CLOCK_SYS_Init(g_clockManConfigsArr, CLOCK_MANAGER_CONFIG_CNT, g_clockManCallbacksArr, CLOCK_MANAGER_CALLBACK_CNT);
	CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_AGREEMENT);

	/* Initialize pins: */
	PINS_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);

	/* Initialize LPSPI0:? */
	LPSPI_DRV_MasterInit(LPSPICOM1, &lpspiCom1State, &lpspiCom1_MasterConfig0);

	/* Set delay for transmission: */
	LPSPI_DRV_MasterSetDelay(LPSPICOM1, 1u, 1u, 1u);

	/* CR95HF initialization: */
	ConfigManager_HWInit();

	/* Main loop: */
	while (1) {

		TagType = ConfigManager_TagHunting(tagfounds);
		if ( TagType == TRACK_NFCTYPE1 ||
			TagType == TRACK_NFCTYPE2 ||
			TagType == TRACK_NFCTYPE3 ||
			TagType == TRACK_NFCTYPE4A ||
			TagType == TRACK_NFCTYPE4B ||
			TagType == TRACK_NFCTYPE5)
		{
			/* Some useful commands to demonstration: */
			if (!flag) {
				drv95HF_SendReceive(Cr95hfCmdInventory, resp);
				drv95HF_SendReceive(Cr95hfCmdWriteSingleBlock, resp);
				drv95HF_SendReceive(Cr95hcCmdReadSingleBlock, resp);
			}
		}
		else {
		}

	}
}

//----------------------------------------------------------------------------//

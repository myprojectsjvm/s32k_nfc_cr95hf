/***************************************************************************//**
	@file		defines.h
	@brief		General defines.
********************************************************************************
	General defines.

	@note		S32 Design Studio for ARM.

	@note		S32K microcontrollers.

********************************************************************************
	@author		Jose Vinicius Melo.
	@date		February-2019.
*******************************************************************************/

#ifndef DEFINES_H_
#define DEFINES_H_

/*******************************************************************************
	SETTINGS:
*******************************************************************************/

#define PORT_CR95HF_SPI_SS			PTA
#define PORT_CR95HF_IRQ_IN			PTA

#define PIN_CR95HF_SPI_SS			0
#define PIN_CR95HF_IRQ_IN			1

#define LPSPICOM_CR95HF				LPSPICOM1

#define delay_ms(x)					OSIF_TimeDelay(x)
#define delayHighPriority_ms(x)		OSIF_TimeDelay(x)

/** @todo CR95HF us delay. */
#define delay_us(x)					OSIF_TimeDelay(x)

/*******************************************************************************
	DEFINES:
*******************************************************************************/

#define IO_CTRL_SET_CR95HF_SPI_SS	PINS_DRV_WritePin(PORT_CR95HF_SPI_SS, PIN_CR95HF_SPI_SS, 1)
#define IO_CTRL_SET_CR95HF_IRQ_IN	PINS_DRV_WritePin(PORT_CR95HF_IRQ_IN, PIN_CR95HF_IRQ_IN, 1)

#define IO_CTRL_CLR_CR95HF_SPI_SS	PINS_DRV_WritePin(PORT_CR95HF_SPI_SS, PIN_CR95HF_SPI_SS, 0)
#define IO_CTRL_CLR_CR95HF_IRQ_IN	PINS_DRV_WritePin(PORT_CR95HF_IRQ_IN, PIN_CR95HF_IRQ_IN, 0)

/* ISO14443A: */
#define PCD_TYPEA_ARConfigA	0x01
#define PCD_TYPEA_ARConfigB	0xDF
#define PCD_TYPEA_TIMERW    0x5A

/* ISO14443B: */
#define PCD_TYPEB_ARConfigA	0x01
#define PCD_TYPEB_ARConfigB	0x51

/* Felica: */
#define PCD_TYPEF_ARConfigA	0x01
#define PCD_TYPEF_ARConfigB	0x51

/* ISO15693: */
#define PCD_TYPEV_ARConfigA	0x01
#define PCD_TYPEV_ARConfigB	0xD1

//----------------------------------------------------------------------------//

#endif /* DEFINES_H_ */

//----------------------------------------------------------------------------//

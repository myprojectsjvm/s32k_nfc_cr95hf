/***************************************************************************//**
	@file		app.h
	@brief		Application used to test the CR95HF NFC reader.
********************************************************************************
	Application used to test the CR95HF NFC reader.

	@note		S32 Design Studio for ARM.

	@note		S32K microcontrollers.

	@see		http://www.doxygen.nl/index.html
	
********************************************************************************
	@author		Jose Vinicius Melo.
	@date		February-2019.
*******************************************************************************/

#ifndef APP_H_
#define APP_H_

#include "Cpu.h"

/*******************************************************************************
	SETTINGS:
*******************************************************************************/

/*******************************************************************************
	DEFINES:
*******************************************************************************/

/*******************************************************************************
	TYPEDEFS:
*******************************************************************************/

/*******************************************************************************
	EXTERNAL VARIABLES:
*******************************************************************************/

/*******************************************************************************
	CONSTANTS:
*******************************************************************************/

/*******************************************************************************
	PROTOTYPES:
*******************************************************************************/

void App_Main(void);

//----------------------------------------------------------------------------//

#endif /* APP_H_ */

//----------------------------------------------------------------------------//

/***************************************************************************//**
	@file		includes.h
	@brief		General includes.
********************************************************************************
	General includes.

	@note		S32 Design Studio for ARM.

	@note		S32K microcontrollers.

********************************************************************************
	@author		Jose Vinicius Melo.
	@date		February-2019.
*******************************************************************************/

#ifndef INCLUDES_H_
#define INCLUDES_H_

#include "Cpu.h"
#include "defines.h"

/*******************************************************************************
	TYPEDEFS:
*******************************************************************************/

typedef uint32_t  u32;
typedef uint16_t u16;
typedef uint8_t  u8;

typedef const uint32_t uc32;  	/*!< Read Only */
typedef const uint16_t uc16;	/*!< Read Only */
typedef const uint8_t uc8;		/*!< Read Only */

/* Enum for ST95 state: */
typedef enum {
	UNDEFINED_MODE=0,
	PICC,
	PCD
} DeviceMode_t;

typedef enum {
	UNDEFINED_TAG_TYPE = 0,
	TT1,
	TT2,
	TT3,
	TT4A,
	TT4B,
	TT5
} TagType_t;

//----------------------------------------------------------------------------//

#endif /* INCLUDES_H_ */

//----------------------------------------------------------------------------//
